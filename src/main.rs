mod lib;

fn main() {
    let random_ayah = lib::get_random_ayah();
    println!("{}", random_ayah.get_text());
}
