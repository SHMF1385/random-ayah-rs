use rand::seq::SliceRandom;

const SURAS: [&str; 114] = [
    "الفاتحة",
    "البقرة",
    "آل عمران",
    "النساء",
    "المائدة",
    "الأنعام",
    "الأعراف",
    "الأنفال",
    "التوبة",
    "يونس",
    "هود",
    "يوسف",
    "الرعد",
    "ابراهيم",
    "الحجر",
    "النحل",
    "الإسراء",
    "الكهف",
    "مريم",
    "طه",
    "الأنبياء",
    "الحج",
    "المؤمنون",
    "النور",
    "الفرقان",
    "الشعراء",
    "النمل",
    "القصص",
    "العنكبوت",
    "الروم",
    "لقمان",
    "السجدة",
    "الأحزاب",
    "سبإ",
    "فاطر",
    "يس",
    "الصافات",
    "ص",
    "الزمر",
    "غافر",
    "فصلت",
    "الشورى",
    "الزخرف",
    "الدخان",
    "الجاثية",
    "الأحقاف",
    "محمد",
    "الفتح",
    "الحجرات",
    "ق",
    "الذاريات",
    "الطور",
    "النجم",
    "القمر",
    "الرحمن",
    "الواقعة",
    "الحديد",
    "المجادلة",
    "الحشر",
    "الممتحنة",
    "الصف",
    "الجمعة",
    "المنافقون",
    "التغابن",
    "الطلاق",
    "التحريم",
    "الملك",
    "القلم",
    "الحاقة",
    "المعارج",
    "نوح",
    "الجن",
    "المزمل",
    "المدثر",
    "القيامة",
    "الانسان",
    "المرسلات",
    "النبإ",
    "النازعات",
    "عبس",
    "التكوير",
    "الإنفطار",
    "المطففين",
    "الإنشقاق",
    "البروج",
    "الطارق",
    "الأعلى",
    "الغاشية",
    "الفجر",
    "البلد",
    "الشمس",
    "الليل",
    "الضحى",
    "الشرح",
    "التين",
    "العلق",
    "القدر",
    "البينة",
    "الزلزلة",
    "العاديات",
    "القارعة",
    "التكاثر",
    "العصر",
    "الهمزة",
    "الفيل",
    "قريش",
    "الماعون",
    "الكوثر",
    "الكافرون",
    "النصر",
    "المسد",
    "الإخلاص",
    "الفلق",
    "الناس"];

#[derive(Default)]
pub struct Ayah {
    sura: u8,
    ayah: u8,
    text: String
}

impl Ayah {
    fn sura(mut self, sura: u8) -> Ayah {
        self.sura = sura;
        self
    }
    fn ayah(mut self, ayah: u8) -> Ayah {
        self.ayah = ayah;
        self
    }
    fn text(mut self, text: String) -> Ayah {
        self.text = text;
        self
    }
    fn ayah_in_arabic(&self) -> String {
        let latin_digits: [&str; 10] = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
        let arabic_digits: [&str; 10] = ["٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩"];
        let mut ayah = self.ayah.to_string();
        for digit in 0..=9 {
            ayah = ayah.replace(latin_digits[digit], arabic_digits[digit]);
        }
        ayah
    }
    pub fn get_text(&self) -> String {
        format!("{} ﴿{}﴾ {}", self.text, self.ayah_in_arabic(), SURAS[(self.sura - 1) as usize])
    }
}

fn read_file() -> String {
    let file_content = include_str!("quran-simple-plain.txt");
    file_content.to_string()
}

pub fn get_random_ayah() -> Ayah {
    let file_content: Vec<String> = read_file().split("\n").map(|s| s.to_owned()).collect();
    let random_ayah: Vec<String> = file_content.choose(&mut rand::thread_rng()).unwrap().split("|").map(|s| s.to_owned()).collect();
    let ayah = Ayah::default()
        .sura(random_ayah[0].parse::<u8>().unwrap())
        .ayah(random_ayah[1].parse::<u8>().unwrap())
        .text(random_ayah[2].to_string());
    ayah
}
